# @superkoders/postcss-print

PostCSS plugin for easier creation of print styles.

## How it works?

Plugin adds `print` media query to those queries, which target medium breakpoints. The result printed page should look like tablet view.

More docs to follow.

## Usage
```
const print = require('@superkoders/postcss-print');

return src(...)
    .pipe(postcss([autoprefixer(), ..., print({ breakpoint: breakpoints?.md || '750px' })])
    .pipe(dest(...)),
);
```