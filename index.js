var postcss = require('postcss');

module.exports = postcss.plugin('postcss-tablet-to-print', ({ breakpoint = '750px' } = {}) => {
	const breakpointValue = parseFloat(breakpoint);

	return (root, result) => {
		return new Promise((resolve) => {
			root.walkAtRules('media', (rule) => {
				const prev = rule.prev();

				if (prev.type === 'comment' && prev.text === '! ignore-print') {
					prev.remove();
					return;
				}

				if (rule.params.includes('print')) return;

				const matches = rule.params.match(/\((.*?)\)/g);
				if ((matches == null) | (matches.length === 0)) return;

				const result = matches.map((find) => {
					const [prop, value] = find.replace(/\s|\(|\)/g, '').split(':');
					const isMin = prop === 'min-width' && parseFloat(value) <= breakpointValue;
					const isMax = prop === 'max-width' && parseFloat(value) > breakpointValue;
					return isMin || isMax;
				});

				if (result.every(Boolean)) {
					rule.params += ', print';
				}
			});

			result.root = root;
			resolve();
		});
	};
});
